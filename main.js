import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import BootstrapVue from 'bootstrap-vue'
import VueResource from 'vue-resource'

// inicio jmv importar lectura de api 
//import axios from 'axios'
//import VueAxios from 'vue-axios'
// fin jmv importar lectura de api 
//Vue.use(axios)
//Vue.use(VueAxios)


import ApolloClient from "apollo-client";
import { HttpLink } from "apollo-link-http";
import { InMemoryCache } from "apollo-cache-inmemory";

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'



Vue.use(BootstrapVue)
Vue.use(VueResource)
Vue.config.productionTip = false


/* generacion nueva */
import VueApollo from "vue-apollo";
Vue.use(VueApollo);

const getHeaders = () => {
  const headers = {};
   const token = window.localStorage.getItem('apollo-token');
   if (token) {
     headers.authorization = `Bearer ${token}`;
   }
   return headers;
 };

 // Create an http link:
 const link = new HttpLink({
   uri: 'http://localhost:3000/',
   fetch,
   headers: getHeaders()
 });
 const client = new ApolloClient({
   link: link,
   cache: new InMemoryCache({
     addTypename: true
   })
 });



 const apolloProvider = new VueApollo({
  defaultClient: client,
})
/* fin de generacion nueva */


new Vue({
  router,
  store,
  apolloProvider,
  render: h => h(App)
}).$mount('#app')
